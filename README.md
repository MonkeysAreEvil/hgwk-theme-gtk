hgwk-theme is a modern looking flat theme for Gtk. The theme is compatible with GTK 3.22. It also includes a GTK 2.0 theme using Murrine engine.

The GTK 3.22 theme is based off of Flat-Plat-light, styled after iris-light.
The GTK 2.0 theme is unchanged from iris-light.

---

Installation Instructions:

Extract the zip file to the themes directory i.e. "/usr/share/themes/"
Alternatively, extract it to the ".themes" directory in your home folder if you want the theme to be applied only to you user.

Use a lxappearance, qtconfig, qtconfig-qt4, systemsettings5 etc as required to set the themes.

---

License: GPL-3.0+
